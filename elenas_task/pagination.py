from rest_framework import pagination


class PostLimitOffSetPagination(pagination.LimitOffsetPagination):
    default_limit = 3
    max_limit = 10


class PostPageNumberPagination(pagination.PageNumberPagination):
    page_size = 3
