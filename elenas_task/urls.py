from django.conf.urls import url

from elenas_task.views import TaskViewSet

task_data_list = TaskViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
task_data_detail = TaskViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    url(r'^task-data/$', task_data_list, name='task-data-list'),
    url(r'^task-data/(?P<pk>[0-9]+)/$', task_data_detail, name='task-data-detail'),
]
