from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Task(models.Model):
    description = models.TextField()
    is_complete = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    def __unicode__(self):
        return u'{}'.format(self.description)

    def __str__(self):
        return self.description
